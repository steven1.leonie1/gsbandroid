L’objectif de cette mission est de faire migrer le site existant de « GSB visiteurs » en Symfony 4 (dernière version stable)
dans un environnement Linux.


**L’application « GSB Visiteurs »** permet aux visiteurs de renseigner et de consulter leurs frais liés à une visite de
médecins. La gestion est la suivante :
- A chaque dépense type (hôtel, repas…) correspond un montant forfaitaire appliqué. On parle de frais
forfaitisés.
- Tout autre type de dépense non référencé dans la base de données (table FraisForfait) fait l’objet d’un frais
hors forfait. Dans ce cas, le visiteur doit fournir les informations suivantes : la date, le montant et le libellé de
la dépense.

L’application est accessible uniquement par les visiteurs à travers l’URL : <site_gsb>/visiteurs.
La page d’accueil est une page de connexion qui permet au visiteur de s’authentifier par son login et son mot de passe.
Une fois authentifié, l’application propose deux fonctionnalités au visiteur :
- La saisie et la modification de ses frais.
- La consultation de ses frais enregistrés.
Le visiteur peut se déconnecter depuis n’importe quelle page.

**Saisie et modification des frais :**

Le visiteur peut renseigner (saisie/modification) sa fiche de frais dès le premier jour du mois. La fiche peut être
modifiée tout au long du mois jusqu’à sa clôture. La clôture d’une fiche est effectuée par l’application « visiteurs » le
dernier jour du mois selon le processus suivant : à la première saisie pour le mois N par le visiteur, sa fiche du mois
précédent (N-1) est clôturée si elle ne l’est pas.
La modification d’une fiche fait appel à la même vue que la saisie. Le visiteur peut y ajouter de nouvelles données ou
supprimer des éléments saisis.
Les frais saisis peuvent remonter jusqu’à un an en arrière (au mois d’août 2019, on peut saisir des frais engagés de
septembre 2019 à août 2020).
Pour saisir une fiche de frais, le visiteur valide la fonctionnalité de saisie. L’application lui permet de saisir le mois
concerné par la fiche. Si la fiche n’existe pas, la fiche est créée.
La page de saisie / modification de la fiche est composée de deux vues :
- Une vue permettant la saisie des éléments forfaitisés. Pour chaque élément, un champ permet la saisie de la
quantité correspondante à l’élément. La validation de ces éléments met à jour la base de données.
- La 2ème vue permet la saisie des éléments hors-forfait. Pour chaque élément hors-forfait, on doit renseigner la
date, le libellé du forfait ainsi que le montant correspondant.
Le cas d’utilisation suivant décrit le processus de saisie :