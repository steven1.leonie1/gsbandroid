package com.android.gsb_compte_rendu;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.gsb_compte_rendu.Manager.DataBaseManager;
import com.android.gsb_compte_rendu.Metiers.CoefConfiance;
import com.android.gsb_compte_rendu.Metiers.Visiteurs;
import com.android.gsb_compte_rendu.PageDesign.DashboardClientActivity;
import com.steven.gsb_compte_rendu.R;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    static String logSend = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DataBaseManager.getInstance(this);
        CoefConfiance coefConfiance = new CoefConfiance(this);
        coefConfiance.setCoef();
    }

    public void checkLogin(View vue){

        Visiteurs vis = new Visiteurs(this);
        vis.open();
        EditText log = (EditText)findViewById(R.id.edLogin);
        EditText pass = (EditText)findViewById(R.id.edPassword);

        String login = log.getText().toString();
        String password = pass.getText().toString();

        if(!login.matches("") && !password.matches("")){

            if(vis.checkAuthentication(login, password)){

                logSend = login;
                Intent intentEnvoyer = new Intent(this, DashboardClientActivity.class);
                startActivity(intentEnvoyer);
            }

            else{
                Toast.makeText(this, "Erreur: Nom d'utilisateur ou mot de passe incorrect", Toast.LENGTH_LONG).show();
            }
        }

        else {
            Toast.makeText(this, "Erreur: Tout les champs doivent être remplis", Toast.LENGTH_LONG).show();
        }
    }

    public static String getLogSend() {
        return logSend;
    }
}
