package com.android.gsb_compte_rendu.PageDesign;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.gsb_compte_rendu.MainActivity;
import com.android.gsb_compte_rendu.Metiers.RapportVisite;
import com.android.gsb_compte_rendu.Metiers.Visiteurs;
import com.steven.gsb_compte_rendu.R;

import java.util.List;

public class AfficherCompteRendueActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_afficher_compte_rendue);
        TextView TvDate = (TextView)findViewById(R.id.TvDate);
        TextView TvMotif = (TextView)findViewById(R.id.TvMotif);
        TextView TvDateVisite = (TextView)findViewById(R.id.TvDateVisite);
        TextView TvBilan = (TextView)findViewById(R.id.TvBilan);
        TextView TvCoef = (TextView)findViewById(R.id.TvCoef);

        Visiteurs visiteurs = new Visiteurs(this);
        visiteurs.open();
        String idMatricule = visiteurs.getMatriculeByLogin(MainActivity.getLogSend());
        visiteurs.close();

        RapportVisite rapportVisite = new RapportVisite(this);
        rapportVisite.open();
        List<String> compteRendue = rapportVisite.afficheRapport(idMatricule);
        rapportVisite.close();


        TvDate.setText(SelectDateActivity.getDateSelected());
        Log.d("IDDDDD", idMatricule);
        Log.d("LISTTTTTT", String.valueOf(compteRendue));


        TvMotif.setText(compteRendue.get(0));
        TvBilan.setText(compteRendue.get(1));
        TvDateVisite.setText(compteRendue.get(2));
        TvCoef.setText(compteRendue.get(3));

    }
}
