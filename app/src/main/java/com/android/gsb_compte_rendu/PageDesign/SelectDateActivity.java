package com.android.gsb_compte_rendu.PageDesign;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.gsb_compte_rendu.Metiers.RapportVisite;
import com.steven.gsb_compte_rendu.R;

import java.util.List;

public class SelectDateActivity extends AppCompatActivity {

    static String dateSelected = null ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_date);

        RapportVisite rapport = new RapportVisite(this);
        rapport.open();
        List<String> datevisite = rapport.getDateVisite();
        rapport.close();

        Spinner date1 = (Spinner) findViewById(R.id.ListeDate);

        ArrayAdapter<String> adaptateurDate = new ArrayAdapter<String>(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item, datevisite);
        adaptateurDate.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        date1.setAdapter(adaptateurDate);
    }

    public void selectCompterendue(View vue){
        Spinner date1 = (Spinner) findViewById(R.id.ListeDate);

        dateSelected = date1.getSelectedItem().toString();

        Intent intentEnvoyer = new Intent(this, SelectionnerCompteRendueActivity.class);
        startActivity(intentEnvoyer);
    }

    public static String getDateSelected() {
        return dateSelected;
    }
}
