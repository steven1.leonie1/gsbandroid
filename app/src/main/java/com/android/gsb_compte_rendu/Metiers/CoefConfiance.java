package com.android.gsb_compte_rendu.Metiers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.android.gsb_compte_rendu.Manager.DataBaseManager;

import java.util.ArrayList;
import java.util.List;

public class CoefConfiance {

    private DataBaseManager dbManager;
    private SQLiteDatabase db;
    private int coef_num;
    private String coef_libelle;

    public CoefConfiance(Context context) {
        dbManager = DataBaseManager.getInstance(context);
    }

    public void open(){

        db = dbManager.getWritableDatabase();
    }

    public void close(){

        db.close();
    }

    public List<String> getCoef_libelle(){

        List<String> listLibelle;
        listLibelle = new ArrayList<String>();

        String sql = "SELECT coef_libelle FROM Coef_confiance";
        SQLiteDatabase db = dbManager.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        while(cursor.moveToNext()){
            listLibelle.add(cursor.getString(0));
        }
        cursor.close();
        db.close();

        return listLibelle;
    }

    public int getCoefByLibelle(String libelle){

        int lib = 0;

        String sql = "SELECT coef_num FROM Coef_confiance WHERE coef_libelle = ?";
        SQLiteDatabase db = dbManager.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, new String[] {libelle});

        while (cursor.moveToNext()){
            lib = cursor.getInt(0);
        }
        cursor.close();
        db.close();

        return lib;
    }

    public void setCoef(){

        if(getCoef_libelle() == null) {
            SQLiteDatabase db = dbManager.getWritableDatabase();
            SQLiteStatement stmt = db.compileStatement("INSERT INTO Coef_confiance (coef_libelle) VALUES ('convaincu'),('hésitant'),('non convaincu')");
            stmt.execute();
        }
    }

}