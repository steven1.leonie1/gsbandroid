package com.android.gsb_compte_rendu.PageDesign;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.android.gsb_compte_rendu.MainActivity;
import com.steven.gsb_compte_rendu.R;


public class DashboardClientActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_client);
        }


    public void sasirCompteRendue(View vue){
        Intent intentEnvoyer = new Intent(this, SaisirCompteRendue.class);
        startActivity(intentEnvoyer);
    }

    public void consulterCompteRendue(View vue){
        Intent intentEnvoyer = new Intent(this, SelectDateActivity.class);
        startActivity(intentEnvoyer);
    }
}



