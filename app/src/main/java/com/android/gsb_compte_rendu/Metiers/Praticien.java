package com.android.gsb_compte_rendu.Metiers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.android.gsb_compte_rendu.Manager.DataBaseManager;

import java.util.ArrayList;
import java.util.List;

public class Praticien {

    private DataBaseManager dbManager;
    private SQLiteDatabase db;
    private int pra_num;
    private String pra_nom;
    private String pra_prenom;
    private String pra_adresse;
    private String pra_cp;
    private String pra_ville;
    private String pra_coefNotoriete;


    public Praticien(Context context) {
        dbManager = DataBaseManager.getInstance(context);
    }

    public void open(){

        db = dbManager.getWritableDatabase();
    }

    public void close(){

        db.close();
    }

    public List<String> getListPraticien(){

        List<String> listPraticien;
        listPraticien =  new ArrayList<String>();

        String sql = "SELECT pra_nom from Praticien";
        SQLiteDatabase db = dbManager.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()){
            listPraticien.add(cursor.getString(0));
        }
        cursor.close();
        db.close();
        return listPraticien;
    }

    public int getPranumByName(String name){

        int num = 0;

        String sql = "SELECT pra_num from Praticien WHERE pra_nom = ?";
        SQLiteDatabase db = dbManager.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, new String[] {name});

        while(cursor.moveToNext()){

            num = cursor.getInt(0);
        }

        cursor.close();
        db.close();

        return num;
    }

}
