package com.android.gsb_compte_rendu.Metiers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.android.gsb_compte_rendu.Manager.DataBaseManager;

import java.util.ArrayList;
import java.util.List;

public class RapportVisite {

    private DataBaseManager dbManager;
    private SQLiteDatabase db;
    private int rap_num;
    private int pra_num;
    private String rap_bilan;
    private String rap_dateVisite;
    private String rap_dateRapport;
    private int coef_num;

    public  RapportVisite(Context context){
        dbManager = DataBaseManager.getInstance(context);
    }

    public void open(){

        db = dbManager.getWritableDatabase();
    }

    public void close(){

        db.close();
    }

    public List<String> getDateVisite(){

        List<String> listeDateVisite;
        listeDateVisite = new ArrayList<String>();
        String sql = "SELECT rap_datevisite FROM Rapport_visite";
        SQLiteDatabase db = dbManager.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        while(cursor.moveToNext()){
            listeDateVisite.add(cursor.getString(0));
        }
        cursor.close();
        db.close();

        return listeDateVisite;
    }

    public List<String> getListeRapports(String dateVisite){

        List<String> listeRapports;
        listeRapports = new ArrayList<String>();
        String sql = "SELECT rap_num , rap_datevisite  FROM Rapport_visite where rap_datevisite=?";
        SQLiteDatabase db = dbManager.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql,  new String[] {dateVisite});

        while(cursor.moveToNext()){
            listeRapports.add(cursor.getString(0));
        }
        cursor.close();
        db.close();

        return listeRapports;
    }

    public void createRapport(String matricule, int praticien, String bilan , String datevisite, String daterapport, int coef){

        String sql = "INSERT INTO Rapport_visite (vis_matricule, pra_num, rap_bilan, rap_datevisite, rap_daterapport, coef_num)VALUES (?,?,?,?,?,?)";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.bindString(1, matricule);
        statement.bindDouble(2, praticien);
        statement.bindString(3, bilan);
        statement.bindString(4, datevisite);
        statement.bindString(5, daterapport);
        statement.bindDouble(6, coef);
        statement.execute();
    }

    public List<String> afficheRapport(String matricule){

        List<String> listeRapports;
        //int itterator = 0;
        listeRapports = new ArrayList<String>();
        String sql = "SELECT vis_matricule, rap_bilan, rap_datevisite, coef_num FROM Rapport_visite WHERE vis_matricule = ?";
        SQLiteDatabase db = dbManager.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, new String[] {matricule});

        while(cursor.moveToNext()){
            listeRapports.add(cursor.getString(0));
            listeRapports.add(cursor.getString(1));
            listeRapports.add(cursor.getString(2));
            listeRapports.add(cursor.getString(3).toString());
        }

        cursor.close();
        db.close();

        return listeRapports;
    }
}
