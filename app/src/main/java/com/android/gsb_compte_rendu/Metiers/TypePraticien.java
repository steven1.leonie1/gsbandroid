package com.android.gsb_compte_rendu.Metiers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.android.gsb_compte_rendu.Manager.DataBaseManager;

public class TypePraticien {

    private DataBaseManager dbManager;
    private SQLiteDatabase db;
    private int typ_code;
    private String typ_libelle;
    private String typ_lieu;

    public TypePraticien(Context context) {
        dbManager = dbManager = DataBaseManager.getInstance(context);
    }

    public void open(){

        db = dbManager.getWritableDatabase();
    }

    public void close(){

        db.close();
    }
}
