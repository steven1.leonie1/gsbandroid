package com.android.gsb_compte_rendu.Metiers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.android.gsb_compte_rendu.Manager.DataBaseManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class Visiteurs {

    private DataBaseManager dbManager;
    private SQLiteDatabase db;
    private String vis_matricule;
    private String nom;
    private String prenom;
    private String adresse;
    private String cp;
    private String ville;
    private String login;
    private String mdp;

    public Visiteurs(Context context){
        dbManager = DataBaseManager.getInstance(context);
    }

    public void open(){

        db = dbManager.getWritableDatabase();
    }

    public void close(){

        db.close();
    }

    /*public String test(String login, String password){

        boolean check = false;
        String log = null;

        String sql = "SELECT * from Visiteur WHERE vis_login = ? and vis_mdp = ?";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.bindString(1, login);
        statement.bindString(2, password);

        Cursor cursor = db.rawQuery(sql, null);

        if(cursor.moveToNext()){
            log = cursor.getString(cursor.getColumnIndex("vis_login"));
        }
        cursor.close();
        statement.close();

        boolean s = false;

        return log;
    }*/

    public boolean checkAuthentication(String login, String password){

        boolean check = false;
        String sql = "SELECT * from Visiteur WHERE vis_login = ? and vis_mdp = ?";
        SQLiteDatabase db = dbManager.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, new String[] {login, password});
        check = cursor.moveToFirst();
        cursor.close();
        db.close();
        return check;
    }

    public String getMatriculeByLogin(String login){

        String matricule = null;
        String sql = "SELECT vis_matricule FROM Visiteur WHERE vis_login = ?";
        SQLiteDatabase db = dbManager.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, new String[] {login});

        if(cursor.moveToFirst()){
            matricule = cursor.getString(0);
        }
        cursor.close();
        db.close();

        return matricule;
    }


}
