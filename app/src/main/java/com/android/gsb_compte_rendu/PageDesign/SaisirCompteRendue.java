package com.android.gsb_compte_rendu.PageDesign;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.gsb_compte_rendu.MainActivity;
import com.android.gsb_compte_rendu.Metiers.CoefConfiance;
import com.android.gsb_compte_rendu.Metiers.Praticien;
import com.android.gsb_compte_rendu.Metiers.RapportVisite;
import com.android.gsb_compte_rendu.Metiers.Visiteurs;
import com.steven.gsb_compte_rendu.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SaisirCompteRendue extends AppCompatActivity {

    Praticien praticien = new Praticien(this);
    CoefConfiance coefConfiance = new CoefConfiance(this);
    Spinner spinnerPraticien;
    Spinner spinnerCoefConfiance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saisir_compte_rendue);

        praticien.open();
        List<String> listPraticiens = praticien.getListPraticien();
        praticien.close();

        spinnerPraticien = (Spinner)findViewById(R.id.ListePractitien);
        ArrayAdapter<String> adapterPraticien = new ArrayAdapter<String>(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item, listPraticiens);
        adapterPraticien.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerPraticien.setAdapter(adapterPraticien);

        coefConfiance.open();
        List<String> listCoefConfiance = coefConfiance.getCoef_libelle();
        coefConfiance.close();

        spinnerCoefConfiance = (Spinner)findViewById(R.id.ListeCoef);
        ArrayAdapter<String> adapterCoef = new ArrayAdapter<String>(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item, listCoefConfiance);
        adapterCoef.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerCoefConfiance.setAdapter(adapterCoef);

    }

    public void ValiderCompteRendue(View vue){

        EditText date = (EditText)findViewById(R.id.TpDate);
        EditText motif = (EditText)findViewById(R.id.TpMotif);
        EditText bilan = (EditText)findViewById(R.id.TpBilan);

        String choixPraticien = spinnerPraticien.getSelectedItem().toString();
        String choixCoef = spinnerCoefConfiance.getSelectedItem().toString();

        String dateV = date.getText().toString();
        String motifV = motif.getText().toString();
        String bilanv = bilan.getText().toString();

        coefConfiance.open();
        int idCoef = coefConfiance.getCoefByLibelle(choixCoef);
        coefConfiance.close();

        praticien.open();
        int pranum = praticien.getPranumByName(choixPraticien);
        praticien.close();

        Visiteurs visiteurs = new Visiteurs(this);
        visiteurs.open();
        String idMatricule = visiteurs.getMatriculeByLogin(MainActivity.getLogSend());
        visiteurs.close();

        RapportVisite rapportVisite = new RapportVisite(this);
        rapportVisite.open();
        rapportVisite.createRapport(idMatricule, pranum, bilanv, dateV, getCurrentDate(), idCoef);
        rapportVisite.close();
        Toast.makeText(this, "Compte rendue validé", Toast.LENGTH_LONG).show();
    }

    public void annuler(View vue){

        Intent intentEnvoyer = new Intent(this, DashboardClientActivity.class);
        startActivity(intentEnvoyer);
    }

    public String getCurrentDate(){
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd");
        String strDate = dateFormat.format(date);
        return strDate;
    }
}
