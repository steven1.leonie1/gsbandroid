package com.android.gsb_compte_rendu.PageDesign;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.gsb_compte_rendu.Metiers.RapportVisite;
import com.steven.gsb_compte_rendu.R;

import java.util.List;

public class SelectionnerCompteRendueActivity extends AppCompatActivity {

    static String compteRendueSelectionner = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectionner_compte_rendue);

        String date = SelectDateActivity.getDateSelected();
        RapportVisite rapport = new RapportVisite(this);
        rapport.open();
        List rapports = rapport.getListeRapports(date);
        rapport.close();
        Spinner spinnerRapports = (Spinner)findViewById(R.id.ListeCompteRendue);

        ArrayAdapter<String> adaptateurRapports = new ArrayAdapter<String>(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item, rapports);
        adaptateurRapports.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerRapports.setAdapter(adaptateurRapports);
    }

    public void afficheCompteRendue(View vue){
        Spinner compteRendue1 = (Spinner) findViewById(R.id.ListeCompteRendue);

        compteRendueSelectionner = compteRendue1.getSelectedItem().toString();

        Intent intentEnvoyer = new Intent(this, AfficherCompteRendueActivity.class);
        startActivity(intentEnvoyer);
    }

    public static String getCompteRendueSelectionner() {
        return compteRendueSelectionner;
    }
}
